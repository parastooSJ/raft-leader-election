package raft

import (
	"assignment4/labrpc"
	"bytes"
	"encoding/gob"
	"fmt"
	"math/rand"
	"strconv"
	"sync"
	"time"
)

type ApplyMsg struct {
	Index       int
	Command     interface{}
}

type LogEntry struct {
	Term int
	Command interface{}
	Index int
}

type Raft struct {
	mu        sync.Mutex          // Lock to protect shared access to this peer's state
	peers     []*labrpc.ClientEnd // RPC end points of all peers
	persister *Persister          // Object to hold this peer's persisted state
	me        int                 // this peer's index into peers[]

	//persist state for all servers
	currentTerm int
	votedFor int
	hasVoted bool
	VotedGrantNum int
	log []*LogEntry

	//volatile state on all servers
	commitIndex int
	lastAppendIndex int

	//volatile state on leader
	nextIndex []int
	matchIndex []int

	//follower = 0 ; candidate = 1 ; leader = 2
	state int

	ToLeaderChan chan bool

	ResetTimeoutChan chan bool

	HeartBeatChan chan bool

	PeerStates []int

}

func (rf *Raft) GetState() (int, bool) {

	var term int
	var isleader bool
	term = rf.currentTerm
	isleader = false
	if rf.state == 2 {
		isleader = true
	}
	return term, isleader
}

func (rf *Raft) persist() {
	w := new(bytes.Buffer)
	e := gob.NewEncoder(w)
	e.Encode(rf.currentTerm)
	e.Encode(rf.votedFor)
	e.Encode(rf.hasVoted)
	e.Encode(rf.log)
	data := w.Bytes()
	rf.persister.SaveRaftState(data)

}

func (rf *Raft) readPersist(data []byte) {

	if len(data) >=1{
		r := bytes.NewBuffer(data)
		d := gob.NewDecoder(r)
		d.Decode(&rf.currentTerm)
		d.Decode(&rf.votedFor)
		d.Decode(&rf.hasVoted)
		d.Decode(&rf.log)

	}

}

type RequestVoteArgs struct {
	State int
	Term int
	CandidateId int
	LastLogTerm int
	LastLogIndex int
}

type RequestVoteReply struct {
	Term int
	VoteGranted bool
	BackTOFollower bool
}

type AppendEntryArgs struct {

	Term int
	LeaderId int
}

type AppendEntryReply struct {
	Term int
	Success bool
}

func (rf *Raft) AppendEntry(args *AppendEntryArgs, reply *AppendEntryReply) {


	if rf.currentTerm > args.Term {
		reply.Term = rf.currentTerm
		reply.Success = false
		return
	}
	if rf.state !=0 {
		rf.gotoFollower()
	}

	if rf.currentTerm < args.Term {
		rf.currentTerm = args.Term
	}


	go func() {
		rf.HeartBeatChan <- true
	}()
	reply.Success = true
	return
}

func (rf *Raft) sendAppendEntry(server int, args *AppendEntryArgs, reply *AppendEntryReply) bool {

	ok := rf.peers[server].Call("Raft.AppendEntry", args, reply)
	rf.persist()
	return ok
}

func (rf *Raft) RequestVote(args RequestVoteArgs, reply *RequestVoteReply) {
	reply.BackTOFollower = false
	if rf.state == 2{
		reply.Term = rf.currentTerm
		reply.VoteGranted = false
		reply.BackTOFollower = true
		return
	}
	if rf.currentTerm>args.Term{
		reply.Term = rf.currentTerm
		reply.VoteGranted = false
		reply.BackTOFollower = false
		return
	}
	if rf.currentTerm<args.Term{
		rf.currentTerm = args.Term
		rf.gotoFollower()
	}
	if rf.hasVoted == true && rf.votedFor !=args.CandidateId{
		reply.VoteGranted = false
		return

	}

	lastEntryIndex := 0
	lastEntryTerm := 1
	if len(rf.log) != 0 {
		lastEntry := rf.log[len(rf.log)-1]
		lastEntryTerm = lastEntry.Term
		lastEntryIndex = lastEntry.Index

	}

	//if receiver log is completed than candidate
	candidateNotComplete := false
	if lastEntryTerm > args.LastLogTerm {
		candidateNotComplete = true
	} else if lastEntryTerm <= args.LastLogTerm {
		if lastEntryIndex > args.LastLogIndex {
			candidateNotComplete = true
		}
	}

	if candidateNotComplete{
		reply.VoteGranted = false
		return
	}

	if !candidateNotComplete {
		if rf.hasVoted == false || (rf.hasVoted == true && rf.votedFor == args.CandidateId) {

			reply.VoteGranted = true
			rf.hasVoted = true
			rf.votedFor = args.CandidateId
			go func() {
				rf.ResetTimeoutChan <- true
			}()
		}
	}
	return

}

func (rf *Raft) sendRequestVote(server int, args RequestVoteArgs, reply *RequestVoteReply) bool {
	ok := rf.peers[server].Call("Raft.RequestVote", args, reply)
	rf.persist()
	return ok
}

func (rf *Raft) Start(command interface{}) (int, int, bool) {
	index := -1
	term := -1
	isLeader := true


	return index, term, isLeader
}

func (rf *Raft) Kill() {
	// Your code here, if desired.
}

func (rf *Raft) gotoFollower() {
	rf.state=0
	rf.votedFor=0
	rf.hasVoted = false
	rf.VotedGrantNum =0

}

func (rf *Raft) gotoCandidate() {
	rf.state=1
	rf.currentTerm +=1
	rf.votedFor = rf.me
	rf.VotedGrantNum = 1
	rf.hasVoted = true
}

func (rf *Raft) gotoLeader() {
	rf.VotedGrantNum = 0
	rf.hasVoted = false
	rf.state = 2
	rf.votedFor = 0
	rf.nextIndex = make([]int, len(rf.peers))
	LenOfLog := len(rf.log)
	for idx :=0; idx < len(rf.peers); idx++ {
		rf.nextIndex[idx] = LenOfLog
	}
	fmt.Print("serever :"+ strconv.Itoa(rf.me)+" elected as the leader in term :"+ strconv.Itoa(rf.currentTerm)+"\n")
}

func (rf *Raft) CheckbeLeader() {
	if float32(rf.VotedGrantNum) / float32(len(rf.peers))  > 0.5 {
		go func() {
			rf.ToLeaderChan <- true
		}()
	}
}

func (rf *Raft) BoardcastRequestVote() {

	for idx := 0; idx < len(rf.peers); idx++ {
		go func(idx_val int) {
			if idx_val == rf.me {
				rf.PeerStates[idx_val] = 1
				return
			}
			lenOfLog := len(rf.log)

			voteArgs := RequestVoteArgs{}
			voteArgs.State = rf.state
			voteArgs.Term = rf.currentTerm
			voteArgs.CandidateId = rf.me
			if lenOfLog != 0 {
				lEntry := rf.log[lenOfLog-1]
				voteArgs.LastLogTerm = lEntry.Term
				voteArgs.LastLogIndex = lEntry.Index
			} else {
				voteArgs.LastLogTerm = 1
				voteArgs.LastLogIndex = 0
			}

			voteReply := RequestVoteReply{}
			voteReply.BackTOFollower = false
			ok := rf.sendRequestVote(idx_val, voteArgs, &voteReply)
			if !ok {
				rf.PeerStates[idx_val] = 0
			} else {
				rf.PeerStates[idx_val] = 1
				if voteReply.VoteGranted {
					rf.VotedGrantNum = rf.VotedGrantNum + 1
					go func(){

						rf.ResetTimeoutChan <- true
					}()
				} else {
					if voteReply.Term > rf.currentTerm  || voteReply.BackTOFollower{
						rf.currentTerm = voteReply.Term
						rf.gotoFollower()
					}

				}
			}
		}(idx)
	}

	if float32(rf.VotedGrantNum) / float32(len(rf.peers))  > 0.5 {
		go func() {
			rf.ToLeaderChan <- true
		}()
	}
}

func (rf *Raft) BoardcastAppendEntry() {

	if rf.state != 2 {
		return
	}

	for idx :=0; idx < len(rf.peers); idx++ {
		go func(idx_val int) {
			if idx_val == rf.me {
				rf.PeerStates[idx_val] = 1
				return
			}
			if rf.state != 2 {
				return
			}

			appendArgs := AppendEntryArgs{}
			appendReply := AppendEntryReply{}

			// default init
			appendArgs.Term = rf.currentTerm
			appendArgs.LeaderId = rf.me


			ok := rf.sendAppendEntry(idx_val, &appendArgs, &appendReply)
			if !ok {

				return
			}

		}(idx)
	}


}

func (rf *Raft) StratToWork() {
	var timeout  = time.Duration(rand.Int63()%500+500) * time.Millisecond
	for {
		time.Sleep(50*time.Millisecond)
		if rf.state == 0 {

			select {
			case <- rf.ResetTimeoutChan:

				timeout = time.Duration(rand.Int63()%500+500) * time.Millisecond
				continue
			case <- rf.HeartBeatChan:

				rf.gotoFollower()
				continue

			case <- time.After(timeout):
				rf.gotoCandidate()

			}
		} else if rf.state == 1 {

			rf.BoardcastRequestVote()

			select {
			case <- rf.ToLeaderChan:
				rf.gotoLeader()
				continue
			case <- rf.ResetTimeoutChan:
				timeout = time.Duration(rand.Int63()%500+500) * time.Millisecond
				continue
			case <- rf.HeartBeatChan:
				rf.gotoFollower()
				continue
			case <- time.After(timeout):
				rf.gotoCandidate()
			}
		} else if rf.state == 2 {


			rf.PeerStates[rf.me] = 1

			rf.BoardcastAppendEntry()

		}
	}
}

func Make(peers []*labrpc.ClientEnd, me int,
	persister *Persister, applyCh chan ApplyMsg) *Raft {
	rf := &Raft{}
	rf.peers = peers
	rf.persister = persister
	rf.me = me

	rf.ToLeaderChan = make(chan bool, 100)
	rf.ResetTimeoutChan = make(chan bool, 100)
	rf.HeartBeatChan = make(chan bool, 100)

	rf.votedFor = 0
	rf.hasVoted = false
	rf.PeerStates = make([]int, len(rf.peers))

	rf.log = make([]*LogEntry, 0)
	rf.commitIndex = 0
	rf.lastAppendIndex = 0
	rf.nextIndex = make([]int, len(rf.peers), len(rf.peers))
	rf.matchIndex = make([]int, len(rf.peers), len(rf.peers))
	rf.currentTerm = 0
	rf.readPersist(persister.ReadRaftState())
	rf.VotedGrantNum = 0
	rf.gotoFollower()
	go rf.StratToWork()


	return rf
}
